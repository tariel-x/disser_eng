\chapter{An overview of the subject area} \label{chapter:subject_review}

In the modern world, it is difficult to imagine a person's life without the Internet. Not only are personal computers and phones connected to the network, but also household appliances and even light bulbs. A lot of operations previously carried out through a personal visit to an organization moved to the Internet:
registration and maintenance of bank accounts, car trade, interaction with government agencies. In addition, the Internet is an important data channel for industry and essential services.

Firstly, it is only natural that the emergence of a large number of services provided via the Internet has led to the need to increase server throughput. The number of Internet users increased by 1114\% in 2000-2019~\cite{InternetWorldStats} and continues to grow. For example, in 2000 it was necessary to develop a software that would be able to support 10,000 competitive connections, so-called ``C10k'' (10k connections) problem. By 2010, this problem had lost its relevance, and another one appeared: now it is necessary to serve simultaneously 10 million user connections, ``C10M'' problem.

Secondly, the development of the Internet has significantly complicated the creation of software components that implement all kinds of services. At the beginning of the active development of the Internet, in the early 2000s, most sites were mostly static, having no interactive interface for a user. However, two decades later, many resources contained complex logic for solving daily work tasks. Even those programs for personal computers that were previously unthinkable for implementation in the browser, such as productivity applications and graphical editors, are available today for working online or has such counterparts.

All this variety of applications and problems solved through the Internet leads to that of implementation, use of different programming languages, concurrent processing technologies. The implementation of a large variety of functions in one product leads to the forced modularity of the software. The range of tasks to be solved in this case is significantly narrowed if a suitable instrument is selected for each module.

Thirdly, the development of Internet technologies in various areas requires high reliability of services, and therefore it increases the cost for providing necessary quality level. All this contributes to the development of new tools and techniques to reduce error number and its fatality for the software. There is a need to accelerate development, performance, to facilitate the decomposition of web services, simplify scaling, which leads to the emergence of new solutions and technologies in the area of development, operation and support of web services.

Web service scaling can be done vertically by adding resources (RAM, processing power) within a single node (server or host). However, the possibilities of increasing the node resources are limited, so it is impossible to scale such a system endlessly. Another way of scaling is horizontal, that is, raising the number of parallel hosts processing client requests.

Often, when scaling horizontally, there is a need to increase the performance of not the entire system, but that of one or several logical blocks only. For example, if an information system has some functions, one of which is execution of any complex calculation, it is obvious that the need for scaling is experienced by the component of complex calculations only.

One of the main ways to solve the raising problem of partial performance is splitting software into components. In a system under heavy load, it is most likely some functions are called more often than others, or some functions require more resources to execute. Such functions should be allocated in separate independent applications, which are easy enough to scale to a greater computing power.

To date, there are several technologies for splitting systems into components and ensuring their interaction:

\begin{itemize}
    \item service-oriented architecture, SOA;
    \item microservice architecture;
    \item serverless computing (Function as a Service, FaaS).
\end{itemize}

On the other hand, when dividing a monolithic application into parts, the connectivity of the components is getting lost, and so are the means for their compatibility and interaction control. The loss of control inevitably results in the possibility of incorrect operation in general and deterioration in the quality of the developed product. To solve this problem in service-oriented architectures, the community has created standards fixing communication protocols. The necessary infrastructure using these protocols has also been created: libraries and frameworks.

However, the situation is different for microservice architectures and serverless computing. For microservices there are various solutions responsible for protocol standardization, automatic documentation generation. For FaaS, some methods have been developed for composing several functions into a single logical component, but without checking the compatibility of individual ones.

Next, we will consider the technology of dividing systems into components more thoroughly.

\section{Service-oriented architecture}
\label{sec:soa}

Preconditions for the emergence of service-oriented architecture appeared in the 1980s, when a CORBA standard for writing distributed applications was considered to be promising~\cite{Rosen2012}. Currently, CORBA is an outdated standard and is not of interest. Since the late 90's it was replaced by service-oriented architecture (SOA).

SOA is an approach to building software and involves the use of loosely coupled, replaceable services and standardized protocols for their communication. A service is a ``visible resource performing a repetitive task and defined by an external instruction''~\cite{IBM_SOA}. In the Internet technology, services are commonly referred to as web services and communicate over HTTP. As a rule, a specific web service is responsible for a certain range of kindred tasks in the information system. These can be tasks related to operations over the data describing objects of the same kind or homogeneous tasks.

The general structure of the service is shown in Fig.~\ref{img:service-structure}. The contract (external instruction from the definition above) of a web service includes the following components~\cite{Rosen2012}:

\begin{itemize}
  \item Service interface
  \item Interface definition
  \item Service access policy
  \item QoS
  \item Information about service load
\end{itemize}

\begin{figure}[ht] 
  \centering
  \includegraphics [scale=0.7] {service-structure.png}
  \caption{Schematic representation of the service.} 
  \label{img:service-structure}
\end{figure}

Services have the following characteristics~\cite{Rosen2012}:

\begin{enumerate}
  \item Modularity. SOA implies the decomposition of a single business process into separate components. Services can be composed into consolidated subsystems.
  \item Encapsulation. The service hides its implementation behind the provided interface.
  \item Connection constraints. A well-designed service has a minimum number of links to other components. The number of links affects the complexity and hence maintenance of the whole system.
  \item Responsibility isolation. Generally, the service works on specific separate entities and does not depend on other components in this work.
  \item Autonomy. A feature that enables the developer to update and maintain the service independently of the rest of the system.
  \item Reuse. A well-designed service can be reused in a variety of subsystems.
  \item Refusal of the state hold. The results of each new operation do not depend on the results of previous operations.
  \item Self-documentation. The service contract should fully describe how to connect to this service and how to use it.
  \item Composability. The service may be linked to other services or may be a composition of other services.
  \item Service policies. Distribution of access rights to the service functions for different users.
  \item Independence from the actual location and implementation language.
\end{enumerate}

An important characteristic of the service is its size, i.e. the number of functions it implements. Choosing the optimal size for the service is a complex task that requires consideration of the following aspects: what the customer service is: user, other internal or external service; protocol type for service communication and network infrastructure performance; the size of the subject area of tasks and others.

Standardized protocols of web services are SOAP, REST, XML-RPC, JSON-RPC, Apache Thrift, gRPC, GraphQL and others. Most of these protocols use HTTP as a transport. For SOAP, there is also the possibility of using SMTP, FTP and others as a transport, but this possibility is rarely used because of the specific purpose of transport protocols. Among the listed communication protocols, SOAP is one of the most popular~\cite{Rosen2012,Richards2015}, and it is added to many programming languages. For example, the platform ASP.NET has automatic generation of client and server code of services based on SOAP interface definition. In order to define the SOAP interface, a WSDL standard has been developed; the documents written within this standard contain descriptions of the transmitted messages provided for remote operation calling.

Since SOA appeared a long time ago, the question of the service composing and checking their compatibility is well studied. There are approved standards for defining call sequence and composition in SOA: WS-BPEL, WS-CDL, BPML, ebXML, OWL-S, WSMF, and others. They are designed mainly to describe business processes. For instance, WS-BPEL enables to define the distribution of access rights when describing business processes~\cite{b-sheng}. However, most of the solutions are focused on SOAP protocol and WSDL standard and leave no chance for use with other protocols, including modern ones, such as gRPC or GraphQL.

In addition to the approved and recognized standards, there are also prototypes: like eFlow~\cite{b-casati}, WISE~\cite{b-lazcano}, SOA4All~\cite{b-lecue} and others~\cite{b-sheng,b-lemos}. Most of them, however, have the same disadvantages. Some approaches (METEOR-S~\cite{b-verma}, BCDF~\cite{b-orriens}, SCENE\cite{b-colombo}) imply the presence of a runtime environment or system that calls the service. In our understanding, such an environment is redundant since the services are large software components with different dependencies, and developing a full-fledged runtime environment that supports a variety of programming languages is an irrationally complex task if taking into account the main goal of the project.

The most suitable for solving the tasks, indicated in the introduction, in the SOA context was the SWORD project~\cite{b-ponnekanti}, which allows defining the interface of the service interaction by means of a simpler and more expressive specification than that of other projects based on XML format. Created specification is automatically checked for compliance with the existing specifications of the running services. The software developed within the framework of the study provides the composition of the service with others.

However, created at the end of the XX century service-oriented architecture does not always meet the modern needs of the industry, such as ease of deployment and that of scaling. That is why, the technology of microservice architecture was introduced, and it will be discussed further.

\section{Microservice architecture}

The next iteration of the modular distributed software architecture development is a microservice architecture. It is a continuation of the service-oriented architecture development. At first glance, the approach is very similar to SOA, all the same requirements that were put forward in section~\ref{sec:soa} are true: modularity, encapsulation, reuse, etc. However, the difference lies in the details.

As at the time the SOA technology appeared due to excessive complexity of CORBA~\cite{Rosen2012}, so the microservice architecture has emerged as a result of the increased complexity of the SOAP, WSDL, and other standards~\cite{Richards2015} for tens of years of operation. Microservices usually perform much fewer functions and have fewer dependencies, which simplifies scaling~\cite{Kratzke2018}. A service covers an entirely separate business process and probably relies on other services when operating, whereas a microservice contains a minimum amount of logic and is not dependent on other services when possible.

Another difference is in the approach to service orchestration and choreography. Service orchestration involves coordinating the work and data exchange between different services using a dedicated central element. Figure~\ref{img:service-orchestration} illustrates the example of the service orchestration.

\begin{figure}[ht] 
  \centering
  \includegraphics [scale=0.7] {service-orchestration.png}
  \caption{Service orchestration.} 
  \label{img:service-orchestration}
\end{figure}

Service choreography implies the work of several services on one task without a central control item. This collaboration can be described as building services into a chain or composition of services. Figure~\ref{img:service-choreography} illustrates the example of the service choreography.

\begin{figure}[ht] 
  \centering
  \includegraphics [scale=0.7] {service-choreography.png}
  \caption{Service choreography.} 
  \label{img:service-choreography}
\end{figure}

A microservice architecture encourages a service choreography, not an orchestration, unlike the SOA approach~\cite{Richards2015}.

Also, the use of microservices imposes certain requirements on the infrastructure: version update automation, the automation of testing, infrastructure for discovering suitable services. Without preliminary preparation of such environment, developing large systems in such a paradigm leads to greater costs than developing of a monolithic system~\cite{Rodger2017}.

Microservices usually use protocols like REST, gRPC for synchronous communication, while AMPQ, JSM, MSMQ are used for asynchronous interaction~\cite{Richards2015}. The available interface (API) of a microservice can be defined using the OpenAPI Specification standard (formerly known as Swagger), which in turn uses JSON-Schema to define the data being transferred.

In conditions where the choreography is mainly used and there is no central element, the composition of the services can be a difficult task. The difficulty lies in the absence of a central storage of addresses of dependent services and it can be solved with the help of the infrastructure solution called ``service discovery''. The idea of service discovery by name or ``service discovery'' does not describe service interaction architecture, but it eliminates the need to manually define service dependencies. This makes service discovery easier to deploy and support~\cite{b-sun}. The essence is to allocate a central registry with information about existing microservices, databases and other shared resources, their location and status. If it is necessary to contact an external service, its address is requested from the registry. The approach makes it easier to scale services because it discards the need to specify the actual addresses of dependencies for each deployed component.

Described in section~\ref{sec:soa} specifications and interface compatibility verification systems are not suitable for a microservice architecture, because, firstly, they are focused on synchronous interaction, mostly on the SOAP Protocol. Secondly, most of them are designed to describe business processes, which does not enable to define minutely the composition of microservices, the functionality of which does not usually cover the whole business process. No solutions were found for microservice composition and compatibility checking~\cite{Richards2015}. Service discovery does not solve the compatibility check problem because the registry contains only information about location and status of services by their name.


Certain control of the types of data transmitted is possible in some streaming data platforms, which are also based on a microservice architecture. For example, the Confluent platform~\footnote{Confluent platform. –– URL: https://www.confluent.io/product/confluent-platform ; Access: 2019-05-12.} combines Apache Kafka message broker, Apache Avro data serialization and validation system, and Kafka Streams data stream management tool, which ultimately allows to flexibly configure the data flows routing based on their types.

A microservice architecture has another drawback: according to one of the definitions~\cite{Rodger2017}, a microservice is an application which source code size does not exceed 100 lines, including libraries. However, in practice this definition is difficult to follow, since even a simple microservice needs configuration, validation of configuration, implementation of REST API or AMPQ of a client, validation of an incoming request, connection to a database. If you use a relational database management system, you may need to implement versioning of the database structure. The above list does not include the logic of the microservice itself, but creates a significant amount of code that is repeated in each service.

The solution to this problem consists in the removal of duplicate code into a plug-in library. Or the duplicate code can be moved to a newly created service runtime environment.

\section{Cloud functions}
\label{sec:cloud_functions}

In 2014 Amazon company introduced AWS Lambda serverless platform (serverless computing platform) within Amazon Web Services cloud platform. The main logical unit in it is a function, which, in fact, is also a microservice, but with some differences~\cite{Eyk2018}:

\begin{itemize}
  \item A microservice is a standalone application having necessary libraries. A function is a code that implements only necessary logic.
  \item Function start, its provisioning, connection to a database are carried out by the platform, which means the function itself is not self-sufficient. The microservice can be run separately, and the function can often be started only within the framework of the serverless platform.
  \item The microservice typically runs in daemon mode, responding to incoming requests. The function is often run only when it is requested to be executed.
\end{itemize}

The differences are shown in table~\ref{tbl:ms_cf_diff}.

\begin{table} [ht]%
  \caption{Differences between microservices and cloud functions.}%
  \label{tbl:ms_cf_diff}% label всегда желательно идти после caption
  \begin{SingleSpace}
      \setlength\extrarowheight{6pt} %вот этим управляем расстоянием между рядами, \arraystretch даёт неудачный результат
      \setlength{\tymin}{1.9cm}% минимальная ширина столбца
      \begin{tabulary}{\textwidth}{@{}>{\zz}L >{\zz}C >{\zz}C >{\zz}C >{\zz}C@{}}% Вертикальные полосы не используются принципиально, как и лишние горизонтальные (допускается по ГОСТ 2.105 пункт 4.4.5) % @{} позволяет прижиматься к краям
          \toprule     %%% верхняя линейка
           & Microservices &
           Cloud functions \\
          \midrule %%% тонкий разделитель. Отделяет названия столбцов. Обязателен по ГОСТ 2.105 пункт 4.4.5 
          Composition & business logic, operations logic, required libraries & business logic \\
          Management & operating system manages as a normal process & FaaS framework runs the function and controls its operation \\
          Operating mode & constantly running daemon & a function starts when needed \\
          \bottomrule %%% нижняя линейка
      \end{tabulary}%
  \end{SingleSpace}
\end{table}

A microservice architecture is just a set of recommendations on the microservice design and their communication. Unlike micriservice architecture, a serverless platform is a fully distributed runtime environment. When developing a serverless system, a developer only needs to implement business logic and the order of individual function interaction. The rest of the work – starting, ending, connecting to the message queue and database, receiving data and returning a response, that is, operations logic - is implemented by the platform. A message broker and database management (DBMS) system are also provided as a service. In fact, the developer only gives a reference in the function definition, that the latter needs a database, and uses its interface in the code of the function, without worrying about the moment when and how the database appears. FaaS is a logical continuation of the trend of maximum transfer of infrastructure administration tasks to providers of relevant services, called cloud.

In summary, a function is similar to a microservice, but often is not self-sufficient, does not store the state, uses the capabilities of the platform to run and communicate. Such computing platform is called FaaS (Function as a Service).

In addition to Amazon, the FaaS cloud platform services are provided by Google (``Google CloudFunctions''), Microsoft (``Azure Functions'') and IBM (``IBM Cloud Functions'' based on the ``Apache OpenWhisk'' platform). In addition to cloud providers, there are also self hosted solutions: Apache OpenWhisk, Fission, OpenFaaS and others.

Web services or IoT systems often use separate, independent functions to perform actions when certain events occur or client requests appear.
However, the cloud functions in the FaaS platform can also be used together to perform sequent actions, which is similar to the flow-based programming~\cite{Morrison2010}. That is, any function, having received the request and processed it, returns the result to the client, and passes it to the further function downstream and so on. For example, in a subsystem that processes data, one component can pass intermediate results to another down the chain. One of the obvious applications of such a composition is ETL processes. Using the FaaS platform it is easy to implement a similar data conversion schema if each step is allocated to one or more functions. The functions work concurrently, independently, do not store the state, return the result of work or an error message. An example of such a data conversion system is shown in figure~\ref{img:etl-example}.

\begin{figure}[ht] 
  \centering
  \includegraphics [scale=0.5] {etl-example.png}
  \caption{Example of the ETL system.} 
  \label{img:etl-example}
\end{figure}

For each of the functions presented in the diagram, there are no restrictions on the choice of the programming language, so the FaaS platform, as well as the microservice architecture, enables to choose the most appropriate language and libraries to implement the necessary functionality.

The benefits of serverless computing:

\begin{enumerate}
  \item No need for infrastructure support in case of using cloud-based FaaS solution providers. The supplier provides the developers with the interface for function code uploading and customizing its provisioning. Usually, DBMS is also a provided resource that is not administered by the developer.
  \item There is no need to implement operations logic, that is, auxiliary code for logging, connecting to databases, etc. Operations logic is applied by the runtime environment, that is, the FaaS platform. The platform provides tools to view log files, transfers data to functions, and connects to the database provided.
  \item Scaling of individual components, which occurs automatically when the load on one of the functions increases.
  \item No costs when idle, because the functions run on demand, when the load comes, for example, at the first incoming HTTP request. If the system requests are not executed, there is no need to work, and the platform automatically reduces the number of running function instances.
  \item Indirect advantage can be the need for a competent division of logic into modules, based on the concept definition. The correct separation of logic simplifies the application architecture and, as a consequence, its support.  
\end{enumerate}

The use of the solution mentioned above also has its drawbacks:

\begin{enumerate}
  \item Less system transparency compared to a monolithic application, due to the removal of business logic components into separate, not directly related components. Under the direct connection here we mean a participation of the components in an integrated software component in a certain programming language.
  \item The complexity of the debugging of functions and the system as a whole, which is expressed in the absence of traditional (for example, GDB) debuggers, that enable the user to control data state when moving from one component's instructions to that of another. However, there are a way to solve this problem and it consists in using tracing systems.
  \item There is no commercial solutions or standards in the field of testing systems built in FaaS frameworks.
  \item An unresolved issue of resources caching that should be provisioned each time a function runs, for instance, when connecting to a database. The need to connect to a database can be caused by the functional requirements of the function. For example, in the Apache OpenWhisk platform, Apache CouchDB is used as a DBMS, in which there is no full-text search without the use of extensions, therefore, it is necessary to use a third-party DBMS, for example, Elasticsearch or PostgreSQL. Initialization of the connection to the database every time the function starts will be an unnecessary operation.
\end{enumerate}

Serverless computing is a young approach, not yet widely known and not widely used. As a result, there are only a few solutions providing the composition of functions.

Within the scope of \textbf{Amazon Lambda} platform, Amazon provides AWS State Machine~\footnote{AWS Step Functions. –– URL: https://aws.amazon.com/ru/step-functions; Access: 2019-05-12.} with its own language, Amazon States Language, describing sequence of functions to run (AWS Step Functions) within a specific task~\cite{Garcia-Lopez2018}.

Language and platform capabilities enable:

\begin{enumerate}
  \item Identification of the function sequence as a sequence of state change. Selection of the next function is also carried out on the basis of conditional statements defined in the workflow schema.
  \item Setting the error handling order that occur within a function or when the platform operates based on the exception mechanism.
  \item Setting the number of data processing retries in case of an error.
  \item Running different functions concurrently.
  \item Conditional statements that select the necessary sequence of functions based on the data transmitted.  
\end{enumerate}

Sequences of functions calling are not functions themselves, they are state descriptions made by an external environment.

In addition to Amazon, IBM also provides the ability to define the composition of functions using a developed Java-Script library and ``IBM Composer''~\cite{Garcia-Lopez2018}~\footnote{IBM Composer; URL: https://github.com/apache/incubator-openwhisk-composer; Access 23-08-2019} functionality built in \textbf{IBM CloudFunctions}. A difference between IBM Composer and AWS State Machine is that the composition in the former is also a function that can participate in a composition. That is, the solution is not an extension of the platform, but a library that can be used when writing new functions in JavaScript calling the functions from the composition. The solution also makes it possible to define conditions, number of function call retries and some other features.

As an alternative to writing functions with the composition for OpenWhisk, there are ``OpenWhisk Sequences''~\footnote{https://cloud.ibm.com/docs/openwhisk?topic\=cloud-functions-actions}, which is a simple mechanism for specifying a sequence of execution without the ability to control its operating.

Microsoft provides two mechanisms for the function composition in \textbf{Microsoft Azure Functions} platform: ``Azure App Logic''~\footnote{https://azure.microsoft.com/ru-ru/services/logic-apps/} and ``Azure Durable Functions''~\footnote{https://docs.microsoft.com/ru-ru/azure/azure-functions/durable/durable-functions-overview}. The first one, just as ``AWS Step Functions'', is a mechanism extending the platform and providing the opportunity to define conditions, cycles, etc.~\cite{Garcia-Lopez2018}. It uses a specific language based on JSON or a graphical editor.

The second mechanism is a counterpart to ``IBM Composer'' and consists of the library for C\# and JavaScript and the extension allowing to call some functions from other ones.

The ``Fn Flow''~\footnote{https://github.com/fnproject/flow} extension used in \textbf{Fn} FaaS framework, also implies platform extending, a special API and a client library for it, which together makes it possible to combine several functions into one. At the moment there is implementation of the library in Java. The obvious advantage, as in the case of IBM Composer and Durable Functions, is using the capabilities of the existing programming language instead of creating your own language. As in the previous cases, the API and the library implies not only launch of side functions, but also processing of conditions, error handling, setting the number of retries in case of an error and other functions.

FaaS \textbf{Fission} platform enables to define the composition of functions using ``Fission Workflows''~\footnote{https://docs.fission.io/workflows/} tool, which is both an extension of the platform and a language based on the YAML markup language. The language includes logical blocks for specifying the sequence of execution, logical expressions and data operations.

\textbf{Apache OpenWhisk} as a platform, used in IBM OpenWhisk, combines functions into the sequence, called ``action sequences''.
However, conditional statements and error handling in OpenWhisk are impossible to formulate.

All these tools can be divided into two general categories: extensions of FaaS frameworks with their own composition language and libraries for existing programming languages which allow to define the compositions calling third-party functions. The comparative characteristics for the above systems are presented in table~\ref{tbl:compisition_comparison}. The main parameters to be compared are highlighted:

\begin{enumerate}
  \item Programming model: library and existing programming language or domain-specific language.
  \item Error handling: if the solution has tools to change the system work in case of an error in any component.
  \item Conditional statements: identifying the sequence of the function work depending on initial data or intermediate results.
  \item Concurrent execution of functions: ability to run two different functions simultaneously, followed by the merger of the outputs.
\end{enumerate}

\begin{table} [ht]%
  \caption{Comparison of composition methods}
  \label{tbl:compisition_comparison}
  \begin{SingleSpace}
      \setlength\extrarowheight{6pt} %вот этим управляем расстоянием между рядами, \arraystretch даёт неудачный результат
      \setlength{\tymin}{1.9cm}% минимальная ширина столбца
      \begin{tabulary}{\textwidth}{@{}>{\zz}L >{\zz}C >{\zz}C >{\zz}C >{\zz}C >{\zz}C@{}}% Вертикальные полосы не используются принципиально, как и лишние горизонтальные (допускается по ГОСТ 2.105 пункт 4.4.5) % @{} позволяет прижиматься к краям
          \toprule     %%% верхняя линейка
          Platform & Name & Model of programming & Error handling & Conditional statements & Parallel function running \\
          \midrule %%% тонкий разделитель. Отделяет названия столбцов. Обязателен по ГОСТ 2.105 пункт 4.4.5 
          AWS Lambda & AWS State Machine & DSL (JSON)                         & $ + $ & $ + $ & $ + $ \\
          IBM Cloud Functions & IBM Composer & Библиотека (JS)                & $ + $ & $ + $ & $ - $ \\
          IBM Cloud Functions & OpenWhisk Sequences & DSL (JSON)              & $ - $ & $ - $ & $ - $ \\
          MS Azure Functions & Azure App Logic & DSL (JSON, графический)      & $ + $ & $ + $ & $ + $ \\
          MS Azure Functions & Azure Durable Functions & Библиотека (JS, C\#) & $ + $ & $ + $ & $ + $ \\
          Fn & Fn Flow & Библиотека (Java, JS)                                & $ + $ & $ + $ & $ + $ \\
          Fission & Fission Workflows & DSL (YAML)                            & $ + $ & $ + $ & $ + $ \\
          OpenWhisk & OpenWhisk Sequences & DSL (JSON)                        & $ - $ & $ - $ & $ - $ \\
          \bottomrule %%% нижняя линейка
      \end{tabulary}%
  \end{SingleSpace}
\end{table}

None of the solutions found enables to make sure in the minimal form that the function will start in the workflow schema with the parameters that are passed to it, i.e. it is impossible to check in advance the compatibility of the data types of the interacting components.

\section{Chapter conclusions}

The chapter explored the fundamentals of the modern approaches to building information systems with the focus on web-applications: SOA (Service-oriented architecture), a microservice architecture, FaaS (Function as a Service). The essence of a service-oriented architecture consists in the removal of separate parts of business logic to separate services. Services are independent components, which enables them to be scaled and modified without affecting the system as a whole. SOA has a rich infrastructure, including data exchange standards, interface definitions, service composition.

Hereinafter a description of the microservice architecture, its strengths and weaknesses are listed. Microservices have less functionality than services in SOA, and this is both their strong and weak side. It can be considered as a strong side since microservices increase flexibility of the development and support of large systems, raise the potential of their horizontal scaling. It is also a weakness, as solid business processes are no longer concentrated in a single service.

The principle of serverless (FaaS) computing has also been described. The advantage of the serverless approach is that it moves not only all the infrastructure needed for microservices to the cloud, but also operations logic responsible for calling and running them. Thus, the necessary logic is implemented in cloud functions. The disadvantages are also connected with the mentioned above, as there is a difficulty in transitioning a working system from one FaaS solution to another. In other respects, the serverless approach has the same merits and drawbacks as the microservice architecture.

Many suppliers of FaaS solutions have necessary infrastructure for the composition of the cloud-based functions. All the composition solutions given can be divided into two general categories: platform extensions using their own composition language, and extensions enabling to call one function from another and, thus, to perform the composition. However, neither the first nor the second allows to perform a type-safe composition. This means it is possible to put functions, working with completely different data, into one sequence.

No other solution to the problem addressed in the introduction was found in the application to serverless approach.
