\chapter{Guarantee of the service compatibility in the microservice architecture} \label{chapter:microservices}

As indicated in the introduction, the construction of systems in the microservice architecture can be accompanied by a discrepancy in the data types of microservice interfaces. In practice, this reveals in a situation where one service transmits data to the next one, which is not able to process correctly the data transmitted, during data processing. It can be either initially incorrect data format or data, which structure is of outdated protocol. To avoid these situations, it is proposed to automate the contract compatibility verification of the interacting services before they start running.

The chapter describes three solutions aimed at solving the problem, the first of which is the contract discovery system (contract discovery). The system develops the infrastructure element ``service discovery'', which is to find a suitable, most accessible dependent component by its name. The revision made includes adding information on running microservice contracts, in addition to their names, to the register of running microservices.

Despite the achievement of this goal, ``contract discovery'' has several disadvantages that complicate the work of an application programmer and do not guarantee the correct implementation of microservices of the declared contract.  In addition, real situations, in which the solution development took place, assumed an emphasis on the event-driven architecture of communication.  In connection with all the above, only event-driven architecture is supported in the modified solution, the message broker acts as the register of contracts. To confirm the efficiency of the idea, a prototype of a system for determining the appropriate message queue was developed which extends the RabbitMQ message broker and uses reflection to generate contracts automatically.

However, due to some reasons, such as the need for a deeper revision of the message broker or the development of a new one, the project of the Anzer service composition language was developed. The implemented software for the language does not guarantee the compatibility of the services and does not contribute to the achievement of the set goal. Despite this, the language offers the opportunity to implement the necessary guarantees in the future.

All three solutions, that automate the contract compatibility verification of interacting services before they start working, will be described further in detail.

\section{The principle of suitable services discovery}

To ensure the integration of microservices and maintain their compatibility, the study developed the principle of suitable services discovery, called ``contract discovery''. The basis was the principle of ``service discovery'', but instead of the name, one uses the description of the interaction interface and types of data exchanged to search for a dependent component.

The basic concept in the ``contract discovery'' approach is the contract that fixes the data exchange protocol as a chain of transmitted value types  between a server and a client within a single data transfer session. Each element of the chain is marked with the ``input'' or ``output'' flag, so you can separate the parameters of the service call from the result of its work.

A typical description of one event in the system is a single contract with one link, which will be marked with the ``input'' flag for services waiting for the event. For services, emitting events, this link will be marked with the ``output'' flag.

In addition to the chain of transmitted data types, the contract contains information about the location of the service providing the contract, and information about the method of checking the relevance of the contract and the description of the transmitted data types. Using type information, a service or some central binding element checks whether the interfaces of the communicating services are compatible.

Example of the contract structure is shown in listing~\ref{list:contract_example}.

\begin{ListingEnv}[!h]
  \captiondelim{ }
  \caption{Example of a contract }
  \label{list:contract_example}
  \begin{lstlisting}
{
    address: http://uri
    healthcheck: http://uri
    chain: [
      {
        direction: in,
        type: A
      },
      {
        direction: out,
        type: B
      }
    ]
}
  \end{lstlisting}
\end{ListingEnv}

The example illustrates the contract defining a synchronous call with two messages:

\begin{enumerate}
  \item The message of the $A$ type which the service expects to receive.
  \item The message, which the service will send as a result, is of $B$ type.
\end{enumerate}

The $address$ field specifies the service address where the input message is expected. The $healthcheck$ field defines the address at which the viability of the service and the implementation of a particular contract is checked.

A service, which implements a certain set of functions and uses the ``contract discovery'' principle, performs registration of the given contracts in the registry at startup. A service, which requires some dependency with a certain interface for operation, also registers the contracts used. If there is information about the interfaces provided and used, as well as when updating this information, the following verification options are available:

\begin{itemize}
  \item Compatibility of services at the moment and meeting all dependencies in the system;
  \item The possibility of updating the service, which entails changing its interfaces;
  \item The possibility of withdrawal of an obsolete service or interface out of operation without risking loss of the dependent part functioning.
\end{itemize}

To meet these opportunities, a central element is needed that performs the following functions:

\begin{enumerate}
	\item Registration of a new supplier (service) contract.
  \item Registration of contract usage by a new service.
  \item Search for suitable providers for services using the contract.
  \item Tracing of contract usage
\end{enumerate}

Thus, a new approach to the service composition in the microservice architecture was developed and it is based on both the principle of ``service discovery'' and the idea of the interface compatibility pre-checking. The new approach is called ``contract discovery''. The contract structure, which contains types of transmitted messages and information about a contract supplier, was described. The requirements to the functional of the projected implementation of the proposed principle were determined.

\subsection{Type system}
\label{section:cc-type-system}

In listing~\ref{list:contract_example}, there are references to types, but there are no definitions. To determine interface compatibility, it requires a description of the types which the modified standard for validation of JSON structures ``JSON-Schema''~\footnote{JSON Schema Validation: A Vocabulary for Structural Validation of JSON. –– URL: http://json-schema.org/latest/json-schema-validation.html ; Access: 2019-05-12.} is used as. The use of the mentioned standard as a basis is due to its wide popularity, and the presence of a large number of libraries and SOFTWARE, implementing various manipulations with schemas.

However, JSON-Schema is a set of data validation rules, and to associate an interface provider with its consumer, one should compare structures of the transferred data to the possibility of those structures extending by a provider. To address this issue, the standard validation has been extended with subtype polymorphism and algorithms for checking correctness of subtyping and type equivalence.

The type system supports subtype polymorphism. Let us assume there is a function a waiting for input data of type $A$, but it is transmitted with data $B$. If type $A$ and type $B$ provided are actually different, and at the same time, the service handles $B$ correctly, it can be concluded that $B$ can be a subtype or a type equivalent to type $A$. That is, $B$ is a subtype of $A$ if every term $B$ can be safely used in a context where $A$ is expected.

For example, listing~\ref{lst:json-inheritance1} describes type $A$, which contains a string type field named \lstinline{f1}. It also describes type $B$, which contains the same string field \lstinline{f1} and, optionally, the integer field \lstinline{f2}. Since type $B$ contains the same fields of the same type as $A$, we can say that $A <: B$. Here, the record $A <: B$ means that $B$ is a subtype of $A$.

\begin{ListingEnv}[!h]
  \captiondelim{ }
  \caption{A and B types}
  \label{lst:json-inheritance1}
  \begin{lstlisting}
A = {
    "type": "object",
    "properties": {
        "f1": { "type": "string" },
    },
    "required": ["f1"]
}
B = {
    "type": "object",
    "properties": {
        "f1": { "type": "string" },
        "f2": { "type": "integer" },
    },
    "required": ["f1", "f2"]
}
  \end{lstlisting}
\end{ListingEnv}

That is, $B$ is a subtype of $A$ if every term $B$ can be safely used in a context where $A$ is expected~\eqref{eq:subtype}~\cite{Pierce2002}.

\begin{equation}
  \label{eq:subtype}
  \frac{ \Gamma \vdash x:A \;\;\; A <: B}{ \Gamma \vdash x : B}
\end{equation}

Inheritance is reflective ($A <: A$) and transitive. For example, listing~\ref{lst:json-transitivity} shows example of types $A$, $B$, and $C$, for which the following is true: $A <: B$, $B <: C$ and $A <: C$. In practice, this means the service, awaiting input of type $A$, will work correctly, receiving data of type $B$ or $C$ in the process, ignoring superfluous fields or restrictions imposed on scalar types.

\begin{ListingEnv}[!h]
  \captiondelim{ }
  \caption{Transitivity}
  \label{lst:json-transitivity}
  \begin{lstlisting}
A = {
  "type": "object",
  "properties": {
    "f1": { "type": "string" },
  },
  "required": ["f1"]
}
B = {
  "type": "object",
  "properties": {
    "f1": { "type": "string" },
    "f2": { "type": "string" },
  },
  "required": ["f1", "f2"]
}
C = {
  "type": "object",
  "properties": {
    "f1": { "type": "string" },
    "f2": { "type": "string" },
    "f3": { "type": "string" },
  },
  "required": ["f1", "f2", "f3"]
}
  \end{lstlisting}
\end{ListingEnv}

Moreover, depth subtyping is true, i.e. the types of each corresponding field of a composite type may vary, but should be in terms of inheritance, as in the listings~\ref{lst:json-inheritance-depth1} and~\ref{lst:json-inheritance-depth2}. In the first example, type $A$ defines the object with one string field \lstinline{f1}, and type $B$ defines the string field \lstinline{f1}, which cannot be longer than 10 characters. Since the names of the fields, their types coincide, and the field \lstinline{f1} of type $A$ does not impose any restrictions on the string, we can assume that $A <: B$.   

\begin{ListingEnv}[!h]
  \captiondelim{ }
  \caption{Depth subtyping 1}
  \label{lst:json-inheritance-depth1}
  \begin{lstlisting}
A = {
  "type": "object",
  "properties": {
    "f1": { "type": "string" },
  },
  "required": ["f1"]
}
B = {
  "type": "object",
  "properties": {
    "f1": { 
      "type": "string",
      "minLength": 10
    },
  },
  "required": ["f1"]
}
  \end{lstlisting}
\end{ListingEnv}

The second example clearly illustrates the depth subtyping in the described type system. Type $A$ in listing~\ref{lst:json-inheritance-depth2} contains field \lstinline{f1}, which is also an object describing the nested field \lstinline{sf1}. Type $B$ describes the same hierarchy of fields and types, except for the fact that object field \lstinline{f1} describes another string field \lstinline{sf2}, which is not provided by type $A$. However, it is assumed that the service awaiting data of type $A$, though having received input data of type $B$, will ignore the field \lstinline{sf2}. Then, $A <: B$ can be considered true.
 
\begin{ListingEnv}[!h]
  \captiondelim{ }
  \caption{Depth subtyping 2}
  \label{lst:json-inheritance-depth2}
  \begin{lstlisting}
A = {
  "type": "object",
  "properties": {
    "f1": {
      "type": "object",
      "properties": {
        "sf1": {"type": "string"},
      },
      "required": ["sf1"]
    },
  },
  "required": ["f1"]
}
B = {
  "type": "object",
  "properties": {
    "f1": {
      "type": "object",
      "properties": {
        "sf1": {"type": "string"},
        "sf2": {"type": "string"},
      },
      "required": ["sf1", "sf2"]
    },
  },
  "required": ["f1"]
}
  \end{lstlisting}
\end{ListingEnv}

Rearranging of fields in the description of the user-defined type does not affect  subtyping.

If there are two types $A$ and $B$, for which $A <: B$ and $B <: A$, they should be considered equivalent. 

Summarizing the above, to ensure the efficiency of the ``contract discovery'' principle, a type system was introduced and described, the basis for which is the method of describing the structure of the transmitted data by means of ``JSON-Schema''. Concepts of subtype polymorphism, reflectivity, and transitivity have been added to the JSON-Schema standard. The syntax has not changed in comparison with the original standard.

\subsection{Implementation and testing}

The prototype of the service, using all compatibility checks and dependency search, is implemented in the PHP programming language and uses Redis as a database. The source code is available in the Github repository~\footnote{Contract controller proof-of-concept. -- URL: http://github.com/tariel-x/cc ; Access: 2019-05-12.}. The final version is implemented in Golang programming language, but it is not freely available. Languages PHP and Golang were selected due to the availability of the corresponding competencies of the author, the presence of the libraries necessary for implementation.

The described approach was used in the implementation of the data conversion system built of asynchronous-communicating microservices. One of the microservices is a REST API and accepts data of a certain type. After receiving the data in JSON format, it validates them and sends it to the next microservice, which is responsible for loading external resources (images, videos, etc.), links to which are contained in the previously received data. The source data, having links to already loaded resources, is transferred to the last microservice, which writes them to the search index and database.

Despite the limited implementation, experience has shown that the ``contract discovery'' service is really able to act as a guarantor of microservices compatibility, and also monitor the current state of the system.

However, the experience of implementation also revealed a number of shortcomings, such as the lack of a mechanism for automatic generation of contracts and their actualization. The need for such functionality is caused by the contract structure complexity due to its versatility when applied to solving real problems. The complexity of the implementation of automatic contract generation is the need to ensure correct operation for very different services:  working synchronously and asynchronously, written in different languages.

Manual editing of contracts of the proposed structure is not an acceptable option due to the need to edit the contract after every change in the microservice interface and due to the human factor.

Finally, the contract discovery service is a central link containing complex logic, which reduces the overall reliability of the system.
Thus, despite the formal achievement of the goal, the developed ``contract discovery'' system can not be recommended for use in the web services development.

\section{Static type checking at the event-driven microservice architecture}

Due to the shortcomings of the ``contract discovery'' service described in the previous chapter, some simplification and enhancement were made.

Taking into account the real conditions in which the study was conducted, it was concluded that the event-oriented model of microservice interaction is more applicable. Owing to this fact, one has refused support of any synchronous model interaction description in favour of the event-driven architecture based on data transfer via a message broker.

The basis of the updated implementation is the same type system and subtype checking algorithm as before.  Although, now the message broker is responsible for the task of type checking and storing. To make the experiment conduction simpler, it is microservice which perform type checking in the first version.

When a new service connects, the message broker should perform a number of checks based on the information transferred to it:

\begin{enumerate}
	\item The service connects to a message channel with a specific name for reading or writing:
	\begin{enumerate}
		\item If a queue with the same name already exists and the type of data to be transferred is specified, the message broker should check that the type, declared by the service when connecting, is a subtype or identical to the type specified for the queue.
		\item If the queue with this name does not exist, the message broker should create it with the type passed by the service.
	\end{enumerate}
	\item If the service does not specify to which queue it is connecting for reading or writing and at the same time it determines the type of the data transferred – the broker should choose the appropriate queue based on information about existing ones.
	\item In all other cases, a connection error should occur.
\end{enumerate}

If the connection is successful, further checks are not performed, the service can transfer the data.  The checks are performed both to try to read from the queue and to write to it.

Given example of the algorithm made it possible to protect the system from typing errors, it is not overcomplicated by excessive versatility, the system is not added with superfluous binding elements.

\subsection{Service update}

The microservice interface update in the given workflow schema may cause difficulties.  If the new service version has an interface incompatible with the previous version, they cannot be started at the same time to work with the same queue. If you first disable previous microservice version, there are still other microservices working with the outdated data type. In this case, they should be disabled.

In a situation when there are no more microservices working with a certain queue, the broker should delete this queue.

\subsection{Implementation}

Among all considered popular message brokers (RabbitMQ, Apache Kafka, NATS) none of them enables to implement any type checking entirely by their own means.  However, RabbitMQ makes it possible to save additional information for queues. There was an implementation developed to be integrated in each microservice in order to check algorithm efficiency.  The library performs all checks instead of the broker, but uses RabbitMQ as a centralized type repository.  Interaction with the message broker is performed not only by means of AMPQ messaging protocol, but also with REST API.

The library implementing the algorithm is developed in Golang\footnote{Typesafe composition. -- URL: https://github.com/tariel-x/tsc;  Access: 2019-05-12.}, uses contract generation and reflection to ensure that declared input and output types of the microservice correspond to the code implemented. Check of the compatibility of the service types and queue types is possible owing to the construction of data type descriptions in JSON-Schema based on microservice source code analysis.

Creating a new service is as follows: initially, there is generation of the service basics, including blank structure description of the  argument and that of the result, and the code that performs the connection to the message broker. The next step is to describe the argument and the result, to apply business logic. When launching a microservice, the library part of it analyses the structures generated and supplemented by the programmer with the help of reflection and builds the description of the contract on their basis. The next step is to ask the broker for all message queues with their type descriptions, and to determine the compatible queue.

\subsection{Testing}

Previously, the ``contract discovery'' principle was used to build a system with the event-driven microservice  architecture. The improved approach testing used the same system as before.

The process of operation and support of such a system has shown that the described approach does not have the shortcomings listed in the previous section. The limitations of the described approach in comparison with those described in the previous article are levelled by the ease of operation and scaling when using the main function – type checking before the service start.

In the described approach, information about interface is extracted from the microservice source code to ensure that the microservice declares exactly the types of data that it actually expects for input and output. However, this is only true for strongly typed languages (e.g. Golang, С++, Kotlin, etc.). The use of languages, in which it is impossible or time-consuming to determine the data type (for instance, JavaScript or Python), leads to the loss of the described advantage. You can mark languages like TypeScript or PureScript, which in themselves imply typing tools in the code, but being translated into JavaScript, which does not have such tools.

Another drawback of the current implementation revealed, while using it, is dependency on RabbitMQ API. At the present time, lists of the current queues of messages and types attached to these queues are obtained through a separate API. This is undesirable because: it requires a separate connection of the service to the broker; REST API is designed primarily to manage the broker, and not to implement the interaction logic.

In summary, the principle of ``contract discovery'' has been simplified by abandoning versatility. The updated workflow schema implies the use of only event-driven microservice architecture, thanks to which we obtain the contract structure simplification. In comparison with the previous workflow schema, the contract in the improved solution contains only information about the data type and the queue name. Thanks to the contract generation from the source code, there is minimal need for additional developer actions when creating a new service or supporting an existing one. The solution was implemented and tested.

\section{The language of microservice composition}

As stated in the previous section, further development of a solution based on storing types in a message broker requires either revision of the existing broker or development of a new one. At the same time, any of these tasks is labour and time consuming compared to the tasks that need to be solved.

An alternative solution to these tasks is to check the compatibility of the interfaces not before the microservice start, but before its launch, followed by the assignment of the necessary data channels to the microservice.  In such a scenario, the correctness of the contract implementation can be ensured by generating the basis of the function.

The composition of microservices can be specified by some domain-specific language.  Given that a microservice can connect to several message queues, can record the result of its work in several message queues, and that the composition involves the transfer of the result of one function to the input of another, the objects of control in this language are no longer microservices, but functions.  A function in this context is a collection of the typed argument and result with a reference to the source code of the microservice that implements logic.  Given this definition, a microservice as a function has only one argument, that is, it listens to only one message queue, and has only one output, that is, it writes to only one message queue. In fact, the programmer has no limit on the number of inputs and outputs of the microservice, but the description of additional inputs is not provided by the language.   Only some of the service constructions  may be an exception.

Based on the tasks and requirements, the language describes two entities: data types and microservices, which will be referred to as functions on the analogy of the functions of the functional programming languages.

\subsection{Type system of the language}

The type system is entirely borrowed from the systems described in section~\ref{section:cc-type-system}. The type is specified using the keyword \lstinline{data} and the JSON document that defines the type. Example of the model describing a human, is shown in listing~\ref{lst:anzer1-person-type}.


\begin{lstlisting}[caption=A human model, label=lst:anzer1-person-type]
data PERSON = {
  "title": "Person",
  "type": "object",
  "properties": {
    "firstName": {
      "type": "string"
    }
  },
  "required": [
    "firstName"
  ]
}
\end{lstlisting}

Apart from all the features and subtyping rules defined earlier, the language of service composition enables the definition of the data type as a product of two other types, as shown in the example~\ref{lst:anzer1-type-multi1}. The listing shows two string data types, indicating first and last name, and a product type, which includes both strings.

\begin{lstlisting}[caption=A product of types, label=lst:anzer1-type-multi1]
data FNAME = {
   "title": "First name",
   "type": "string"
}
data LNAME={
   "title": "Last name",
   "type": "string"
}
data PERSON=FNAME&LNAME
\end{lstlisting}

The \lstinline{PERSON} type should be then interpreted as shown in listing~\ref{lst:anzer1-type-multi2}, which demonstrates that the merged data type must contain both fields.

\begin{lstlisting}[caption=A product of types, label=lst:anzer1-type-multi2]
data FNAME = {
    "title": "First name",
    "type": "string"
}
data LNAME={
    "title": "Last name",
    "type": "string"
}
data PERSON={
  "type": "object",
  "properties": {
    "FNAME": {
      "title": "First name",
      "type": "string"
    },
    "LNAME": {
      "title": "Last name",
      "type": "string"
    }
  },
  "required": [
    "FNAME",
    "LNAME"
  ]
}
\end{lstlisting}

Additionally, the empty data type \lstinline{data EMPTY_TYPE = _}  is introduced, which means that the service does not wait for any incoming data. A product of an empty and any other type, such as \lstinline{data ABSURD=PERSON&_}, will result in an error.

\subsection{Function definition}

In addition to types, the language, as you can see from the name, implies defining services and combining them in a sequence of execution. In the context of the proposed language, services are denoted by functions. A function can be defined as a reference to the existing service or as a composition of other functions.

In both cases, you should specify the input type for the function as shown in listing~\ref{lst:anzer1-f-typedef}. Provided example demonstrates the name \lstinline{lname} of the function, than after \lstinline{::} it demonstrates the argument type \lstinline{SOURCE} and the result type \lstinline{LNAME}.

\begin{lstlisting}[caption=Definition of the argument and function result, label=lst:anzer1-f-typedef]
lname :: SOURCE -> LNAME
\end{lstlisting}
  
If \lstinline{lname} is a function pointing to a specific service, it is necessary to specify an optional description, as shown in the listing~\ref{lst:anzer1-f-service}.

\begin{lstlisting}[caption=Function as a service., label=lst:anzer1-f-service]
lname :: SOURCE -> LNAME {
  "env_input": "INPUT",
  "env_output": "OUTPUT",
  "env": {
    "DB": "db://localhost"
  }
}
\end{lstlisting}

The main difference of the link to the service is curly brackets, inside which there are indicated:

\begin{itemize}
  \item \lstinline{env_input} -- the name of the environment variable, to which the name of the message queue, to which the service needs to connect to, will be written.
  \item \lstinline{env_input} -- similarly for the queue to which you want to write the result of the work.
  \item \lstinline{env} defines a list of other variables of the environment, in which the service will be launched.
\end{itemize}

All listed items are optional. The example shows the default values for the \lstinline{env_input} and \lstinline{env_input} parameters. The \lstinline{env} dictionary is empty by default.

After the definition, the functions, for which there are no curly braces, should be defined as a composition of other functions. For example, listing~\ref{lst:anzer1-f-composition} defines the \lstinline{main} function  main as a sequence of the \lstinline{lname} и \lstinline{useraction} functions. The main function \lstinline{main} is required in the schema and is used to define the functions that will be run. The order of services in the sequence is defined from right to left. Thus, the \lstinline{lname} service will be executed first, and the result of its work will be passed to the \lstinline{useraction} service.

\begin{lstlisting}[caption=Function composition., label=lst:anzer1-f-composition]
main = useraction . lname
\end{lstlisting}

Apart from defining the sequential function operation, parallel processing can be defined.   To do this, one need to enclose the functions, run in parallel, in angle brackets, as shown in listing~\ref{lst:anzer1-f-parallel}. According to the schema shown in the listing, the \lstinline{init} function will work first, the result of which will be passed to the \lstinline{lname} and \lstinline{fname} functions. The latter ones, in turn, will process it concurrently and return the result.

\begin{lstlisting}[caption=Composition of the functions., label=lst:anzer1-f-parallel]
demonstrationFunc :: _ -> PERSON
demonstrationFunc = <fname,lname> . init
\end{lstlisting}

Since there are two functions, so the results should also be two, so, to unite them, a type product is used. The \lstinline{PERSON} type in the example is defined earlier in listing~\ref{lst:anzer1-type-multi2} as the product of \lstinline{FNAME} and \lstinline{LNAME}. Thus, the results of the two functions are combined into one data block, which can be passed on further to the next function.

\subsection{Implementation}

Working with the schemas written in the Anzer language is carried out using the utility developed in the Golang language. The ANTLR library is used to build and process the source code syntax tree.

The project uses Docker containerization system to run services. All function names used in the language are Docker-image names. When you start the service, a container is created from the image which is named after the function in the schema. Environment variables that are defined by the schema are also assigned to the container.

To run the schema in the Anzer language, you use the Docker compose package manager, which operates Docker containers and uses its own format for describing their orchestration.

When analysing the source code, the first step is to check the compatibility of all function signatures. If no contradictions were found in terms of types, the internal view of all services composition is to be constructed. The internal view does not contain data type descriptions, and operates with services instead of functions. If a function is used more than once, several services with different names are created for it.

In addition to being user-defined, the internal view includes the infrastructure services required to support some language features. One of such services is ``anzer-product'', which is responsible for converting the results of parallel operation of several services into one data block. The type of data block and the result of this service is defined as the product of the result types of all services defined in the current block \lstinline{<a,b,c>}. The service ``anzer-product'' is a universal, written once by the component, which is configured individually for each product of services when constructing the internal view.

The internal view in the JSON format is saved as a separate file

The next step is a separate utility, also developed in Golang, which converts the generated file into a Docker compose schema file. When generating a schema, infrastructure components, such as RabbitMQ message broker, can be added into it. The converted file can be used to run the service pack using the standard Docker compose tools.

It should be noted that the developed software does not interact with the code base of the services. Also, the developed software does not limit the use of the message broker. Despite the fact that the current version of the software is focused on RabbitMQ message broker, there are no restrictions on the use of other brokers.

Therefore, the order of the system using Anzer is as follows:

\begin{enumerate}
  \item Creation of an interaction schema in Anzer and verifying its correctness.
  \item Development of microservices.
  \item Conversion of the interaction schema into a file of the internal view.
  \item Conversion of the internal view into a Docker compose file.
  \item Running containers by means of the \lstinline{docker-compose} utility.
\end{enumerate}

\subsection{Testing}

The concept, language and software were tested during the re-development of the ETL system described earlier. Since the system already used RabbitMQ to exchange data between microservices, the change to use it with Anzer did not cause any difficulties.

The use of the language, as expected, forms an understanding of how the system is made of microservices right away, and partly implements the service compatibility check.  However, due to the lack of connection between the service source code and the description of the type in the interaction schema in the current implementation, Anzer does not guarantee correct work of the services.  The absence of this guarantee does not enable to use the system developed for marginally important tasks.

The only foreseeable way to implement the connection between the source code and the schema is to generate the source code of the services from the Anzer type descriptions.  The most convenient option is the generation of structures and data types for the argument and result, and generation of the service operations logic code.  It is assumed that it is optimal to first describe the types and functions in the Anzer language, then generate the basis for the services. Next, the developer should implement business logic on top of the ready-made operations logic, and publish the service to some repository.

However, generating the service basis is not a protective measure from a mismatch of the service with the schema as the developer can modify the generated types and break their consistency. An alternative solution is to generate operations logic when you deploy the service.  At the same time, the type generation at the last stage complicates the creation of business logic. Additionally, it introduces the need for the development of a special tool for assembling microservices and possibly their deployment and configuration.

The implementation of such functionality leads to the creation of a platform for the development and support of systems made in the microservice paradigm. It is worth noting that similar systems already exist.

\section{Chapter conclusions}

The chapter describes three developed approaches to the organization of type-safe interaction of microservices within one system.	Initially, one considered the support of any forms of service interaction: asynchronous and synchronous. The solution was presented to describe the schema of interaction with the service, to describe the types that the services operate. The service that implements the ``contract discovery'' principle and searches for a component for interaction, based on the required interface, was implemented. Implementation and use of the approach enables to avoid errors associated with the mismatch of the necessary and implemented interfaces during the development and support of a microservice set.

Creating and updating contracts for protocols, based on RPC, requires writing a lot of duplicate code, which is also a disadvantage. The decision also does not provide verification of the fact that the implementation of the interface actually corresponds to the declared contract. That is, there is no connection between the code and the interface description.

Given the conditions in which the study was conducted, support for synchronous interaction was considered unreasonable. Therefore, to simplify and correct these shortcomings, the principle of ``contract discovery'' has been modified to a system that supports only the event-driven communication model. The modified system contains dynamic generation of contracts based on the source code of services. The resulting system appeared to be simpler and more reliable to operate. Tools for code generation and reflection enable to ensure compliance of the interface description with its implementations. However, such tools operate only with those programming languages for which there is a possibility of static type checking. Thus, a preliminary check of data types transferred by microservices for compatibility before their launch or update was implemented. This helped to avoid errors related to data types during the development of new components.

However, extracting the data type from the source code and dynamic generation of contracts before the program starts is not available for all programming languages. The use of source code generation from a generic description of the types is a more preferable option because of the simpler implementation, and support of the most part of programming languages. 

Additionally, the presented library uses non-standard for microservices API RabbitMQ’s capabilities that require separate authorization and are not meant for using in the business logic implementation. Therefore, industrial implementation will require replacing the existing implementation with a full-fledged message broker with support of the typing.

Taking into account the described shortcomings, the Anzer service composition language was developed as an alternative solution.  The language features allow to describe the types of these arguments and the results of services in a universal form, as well as to set their communication. The Docker compose package manager is used to deploy services in the current version, taking into account the interaction schema.

However, as a prototype, the Anzer language and software do not implement any connection between the interaction schema and the source code of services. Thus, the use of Anzer does not guarantee the compatibility and correct operation of microservices during their development. Nevetheless, the use of the language opens up the possibility to generate the source code of the service operations logic from the generic type description. The generated operations logic together with the type descriptions in the target programming language can partially solve the problem of the necessary guarantee.

The disadvantages of generating operations logic include the need to create a specialized platform having tools for code generation, assembly, deployment, and configuration of services. It is worth noting that similar systems already exist and are called serverless platforms.

Thus, the Anzer language partially solves the problems in relation to microservice architectures. However, it is more correct to continue the development of the language in the context of serverless platforms.
