\chapter{A function composition description language in serverless platforms} \label{chapter:anzer}

Discussed in the previous chapter systems and approaches, based on the contract and the contract controller, guarantee the operability of a new service or a new version of an existing one and its compatibility with previously launched ones before its inclusion in the system.  However, due to shortcomings, their use in working systems can be difficult, so the chapter ``Static type checking in a microservice event architecture'' presents the domain-specific language of service composition called Anzer . The proposed language seems to be a promising area of research, allowing to describe the interaction of components in the microservice structure in a simple form, to check their compatibility, but the language also has some disadvantages.

The Anzer language and the accompanying software platform have no connection to the code of operations logic and business logic of a microservice. Basic code generation of a microservice from a schema in the Anzer language can ensure the connection, however this does not solve the problem associated with the change in the process of development of the code, responsible for input-output data serialization. Changing these methods, structures, interfaces and types without changing the interaction schema leads to loss of system component compatibility control.

The problem of synchronizing changes in the schema and code can be solved by two-step code generation: when creating or updating a service and when building a container with the service. The second stage of generation and subsequent compilation or static analysis of the concatenated code would make sure that the microservice is actually correctly implemented and does not violate compatibility.  In addition, re-generation of the code should not differ from the first in the main, since the same type descriptions are generated.

However, such a thorough check of compatibility and implementation requires creation of the developed infrastructure that controls the assembly and launch of microservices.  The software for Anzer already indirectly affects microservice launch parameters.  Two-stage code generation requires the generation of operations logic, as it is difficult to implement a re-generated structure, method or interface in a ready-made service. As a result, the further development of the Anzer language in relation to microservices leads to the emergence of its own new platform for building microservice architectures.

At the same time, the technology of cloud functions, or in another way called ``function as a service'' (FaaS, Function as a Service) is gaining popularity. The essence of the technology, as described in the сhapter ``An overview of the subject area'', is to transfer the entire operations logic of the service to some cloud system, for the work of which the supplier of this system is responsible. Platforms running cloud systems are called FaaS platforms. A broader term is a ``serverless platform'' (serverless computing platform), which refers to the complex of all platforms, implying lack of necessity for the management of computing resources and the development of operations logic.

Such systems imply the use of frameworks for function creation. Moreover, they often include the ability to compose functions, that is, to build logic from individual microservices within a single runtime environment.  Finally, FaaS is a promising and popular technology that partially replaces microservice architectures~\cite{roberts_serverless}.

Given all of the above, it is logical to change the focus of research from microservice architectures to FaaS-systems because:

\begin{enumerate}
  \item Systems, based on the use of cloud functions, are a logical continuation of microservice architectures and considered to be the development of the trend implying transfer of the responsibility for code functioning to the cloud. At the same time, due to the removal of operations logic, microservices become smaller.
  \item Integrating static typing of inter-service communication into one or more FaaS platforms is a simpler task than developing a counterpart of such platforms.
\end{enumerate}

This chapter discusses the domain-specific cloud function composition language called Anzer: syntax, type system, basic constructs. The language enables to define the types of data transferred between functions. It also enables a composition of functions into one abstraction and the verification of their compatibility. To work with the language, a specialized platform is presented, which includes a language analyser, a client for Apache OpenWhisk FaaS platform and a library for the same FaaS platform. Golang was chosen as the first target programming language, and the platform was also implemented on it. Together, the language and platform allow to implement a system built from cloud functions and ensure that the functions implemented are compatible.

\section{The language of function composition}

Functions in FaaS platforms run in case of need, perform the programmed action and pass the result forward, not keeping the state. This feature partly creates an affinity between the functions mentioned above and the concept of functions from some functional programming languages.

In addition, if we consider a set of functions as a single monolithic program, then, due to the absence of a global runtime environment and mutable variables, drawing an analogy with imperative programming languages is impossible. Functional approach, on the contrary, is characterized by a composition of independent functions, which means the result of the calculation of the previous function is applied to the next one. Also, programs written in a functional style only do not contain mutable variables, and functions can be easily moved from one program to another.

Listed properties create an affinity between software, made in the frames of the functional paradigm, and systems, built using FaaS platforms. In this regard, the concepts of such functional languages as Haskell and PureScript were taken as the basis for the proposed language.

The language allows to specify the characteristics of the types of data transmitted, the sequence of functions running, the order of error handling. With the help of  ``\lstinline[language=haskell]{//}'' character there is an opportunity to write comments in the code.

\subsection{Type system}

The type system as a whole is based on the system described in chapter~\ref{chapter:microservices} and contains both basic types (String, Boolean, etc.) and user-defined types, as shown in listing~\ref{lst:complextypedef}.

\begin{lstlisting}[language=haskell, caption=User-defined type, label=lst:complextypedef]
type Address = {
    house   :: Integer
    street  :: Maybe String
    city    :: MinLength 10 String
    country :: String
}
type Addresses = List Address
\end{lstlisting}

Also, the type system of the language developed supports extension of basic and user-defined types with the help of type constructors. \lstinline{List} is a type constructor, that is, a function that converts \lstinline{Address} type to \lstinline{List Address} which is an array of addresses. The constructor \lstinline{MinLength 10} defines a string with a minimum length of 10. You can use \lstinline{Maybe} constructor to determine that a field may not be present in the data being passed. The type with the applied constructors is a new type. Other type constructors are defined by the language.

The Anzer language's type system supports subtype polymorphism just as the ``contract discovery'' type system. Let us assume there is a function a awaiting input data of type $A$, but it is transmitted with data $B$. If type A and provided type $B$ are actually different, and the function will handle $B$ correctly, it can be concluded that $B$ can be a subtype or type equivalent to type $A$.

For example, listing~\ref{lst:inheritance1} describes type $A$, which contains a field of a string type named \lstinline{f1}. It also describes type $B$, which contains the same string field \lstinline{f1} and, optionally, an integer field \lstinline{f2}. Since type $B$ contains the same fields of the same type as $A$, we can say that $A <: B$. 

\begin{lstlisting}[language=haskell, caption=A and B types, label=lst:inheritance1]
type A = {
    f1 :: String
}
type B = {
    f1 :: String
    f2 :: Integer
}
\end{lstlisting}

The inheritance is as reflective as the type system from the chapter ``Guarantee of the service compatibility in the microservice architecture'' $A <: A$. Transitivity is also supported, as shown in listing 3.3 providing the example of types $A$, $B$, and $C$, for which the following is true: $A <: B$, $B <: C$ and $A <: C$.

\begin{lstlisting}[language=haskell, caption=Transitivity, label=lst:transitivity]
type A = {
    f1 :: String
}
type B = {
    f1 :: String
    f2 :: String
}
type C = {
    f1 :: String
    f2 :: String
    f3 :: String
}
\end{lstlisting}

Depth subtyping is also true, as shown in listings~\ref{lst:inheritance-depth1} and~\ref{lst:inheritance-depth2}.

\begin{lstlisting}[language=haskell, caption=Depth subtyping 1, label=lst:inheritance-depth1]
type A = {
    f1 :: String
}

type B = {
    f1 :: MinLength 10 String
}
\end{lstlisting}

\begin{lstlisting}[language=haskell, caption=Depth subtyping 2, label=lst:inheritance-depth2]
type A = {
    f1 :: {
      sf1 :: String
    }
}

type B = {
    f1 :: {
      sf1 :: String
      sf2 :: String
    }
}
\end{lstlisting}

Rearranging of fields in the description of the user-defined type does not affect subtyping.

If there are two types $A$ and $B$, for which $A <: B$ and $B <: A$ are true, they should be considered equivalent.   

It should be noted that the application of some constructors to any type forms its subtype. For example, let type $A$ be the basic string type \lstinline{type A = String}, and type $B$ be the basic string type with the maximum string length constraint constructor \lstinline{type A = MaxLength 10 String} applied. Then, $A <: B$ is true. The language defines \lstinline{List}, \lstinline{Maybe} and \lstinline{Either} and some other constructors which do not form a subtype because of their more complex structure.

Applying different constructors or just one with different parameters leads to the creation of two new different types. For example, types \lstinline{type A = MaxLength 10 String} and \lstinline{type A = MaxLength 20 String} may not be in terms of inheritance or equivalence.

As a result, based on the extension of JSON-Schema standard described in chapter~\ref{chapter:microservices} a new type system has been designed by simplifying the syntax, but preserving the basic features. The rejection of JSON syntax in favour of our own one greatly simplifies type description and allows to easily expand it in the future.

\subsection{Functions}
\label{sec:functions}

The types in Anzer are used to describe function’s arguments and results of its operation. A function is either a reference to a repository with its source code or a synonym for the composition of other functions, that is, a virtual function. Therefore, Anzer does not provide writing the application logic, enabling only a type-safe composition of functions, which have been implemented in other languages. Example of the system description is shown in listing~\ref{lst:example}.

\begin{lstlisting}[language=haskell, caption= Example of the system description in the Anzer language., label=lst:example]
type RawAddress = MinLength 10 String
type Address = {
    street  :: Maybe MinLength 10 String
    city    :: MaxLength 20 String
    country :: String
}

github.com/u/parse[go]:: 
    RawAddress -> Address
isp github.com/u/isprovider[go]:: 
    Address -> Bool

detect :: RawAddress -> Bool
detect = isp . parse

invoke (
    detect,
)
\end{lstlisting}

In the example, the record \lstinline{github.com/u/parse} is a reference to a source repository with function's source code, and \lstinline{RawAddress - > Address} is a specification of the input and output data types. The record \lstinline{isp . parse} defines the composition of the \lstinline{isp} and \lstinline{parse} functions, while \lstinline{detect} is a function defined as the composition of the other two. Composition \lstinline{detect = isp . parse} indicates that the data of a user request or other event will be first received by the \lstinline{isp} function. The \lstinline{detect} function will take the intermediate result. And the result of the last function in the composition will be returned to the user or event which initiated the start.

The \lstinline{invoke} keyword determines, which functions will be deployed in the FaaS platform. In this case, \lstinline{detect} is synonymous with the composition of other functions: \lstinline{isp} and \lstinline{parse} and, consequently, the latter ones will be deployed. At the end of the \lstinline{isp} operation there will be an event created in the system and containing result of its operation, by means of which the \lstinline{parse} function will be launched and receive this result.

Thus, the language is focused on the specification of the call sequence of the FAAS platform functions and data transfer between them.
To construct complex schemas, there is a means of abstraction implying the definition of a function as a composition of others. It is assumed that when analysing the source code in the Anzer language, all virtual functions are turned into the sequence of the function composition of the FAAS platform.

\subsection{Error handling}

To handle errors it is possible to use the type constructor \lstinline{Either a b = Left a | Right b} ,which specifies that the function returns data of either type \lstinline{a} or type \lstinline{b}. For example, \lstinline{Either Error Result} defines an algebraic data type which means that the result can be either of \lstinline{Error} type or \lstinline{Result} type. There is no predefined error type.

The composition of functions which returns the result \lstinline{Either Error Result} with functions expecting only the \lstinline{Result} type is performed using the \lstinline{Either} monad. Its definition and use are similar to that of the Haskell programming language~\cite{Haskell-error}. Construction \lstinline{Either} in Anzer defines two operations: binding (\lstinline{>>=}) and wrapping (\lstinline{return})

The binding operation is a higher-order function of Anzer; it takes some data and another function as arguments and is defined as follows:

\begin{lstlisting}[language=haskell]
Right a >>= f = f a
Left a >>= f = Left a
\end{lstlisting}

Given example shows that if the first argument of the function \lstinline{>>=} is of type \lstinline{Right a}, where \lstinline{a} is a user-defined data type, then \lstinline{>>=} converts data of type \lstinline{Right a} to type \lstinline{a}. Then, the function \lstinline{f} passed by the second argument is applied to the given data, and the result of this application is the result of the operation \lstinline{>>=}.

If the first binding argument is of \lstinline{Left a} type, the operation returns the data passed to it unchanged.

Thus, using the \lstinline{>>=} operation it is possible to bind functions, which return an error message instead of operation result, with functions, which expect only correct data, not an error. At the same time, once generated, the error will reach composition's end without changes.

Another operation of Either monad is \lstinline{return} function, which can be defined as follows: \lstinline{return a  = Right a}. As you can see from the definition, \lstinline{return} converts user-defined data of type \lstinline{a} to type \lstinline{Right a} by means of the \lstinline{Right} constructor. With the help of this operation you can bind functions that return type \lstinline{a} instead of \lstinline{Right a}.

Due to the fact that the language does not support a predefined type for an error, there is no integration with the error handling tools existing in some FaaS platforms.

Since Anzer is a domain-specific language, not a general-purpose language, there is no provision for creating custom type constructors, monads, or higher-order functions.

Thus, the language enables to describe a specialized error type and determine which functions can return an error instead of the correct result.  

A defined by the language monad of exception and higher-order functions can pass the information about the error to the function that can handle it.

\section{Anzer Platform}

One composition language would not be enough to achieve the goal, so the proposed solution also includes a specialized platform, the tasks of which include:

\begin{itemize}
  \item Type checking in the description of the function composition in the Anzer language.
  \item Verification of conformity of the function implementation to its description in the language of Anzer.
  \item Build of functions into executable files or into a format suitable for the deployment to the cloud platform.
  \item Deployment of new functions versions in the selected FaaS platform.
  \item Set up of the connection between functions.
\end{itemize}

\subsection{General organization}
\label{chapter:anzer:structure}

To understand the requirements set for the software implementation, it is necessary to clarify the order of its use. The typical process of creating a new system in the FaaS platform using the proposed solution is as follows:

\begin{enumerate}
  \item Description of the necessary types of data to be transferred, of the supposed functions and their interaction. The software should be able to validate the composition schema before the functions get implemented.
  \item Generation of a basis for necessary functions from a schema in the Anzer language. It should be noted that the basics do not contain operations logic.
  \item Implementation of business logic of functions and their deployment to the repository.
  \item System deployment via the Anzer user interface. What happens alongside:
  \begin{enumerate}
    \item Validation of the composition schema for type compatibility.
    \item Generation of operations logic and the duplicating description of function interfaces on the basis of the described schemas.
    \item Compilation or static analysis of the implemented function and code generated in the previous step.
    \item In case of successful compilation or analysis –- the deployment of functions in the FaaS platform.
  \end{enumerate}
  \item Set up of connections between functions.
\end{enumerate}

To provide the described process of use, it is necessary to develop the following components of the platform:

\begin{enumerate}
  \item A component that parses Anzer language into intermediate representation.
  \item A module of check for schema correctness.
  \item A module responsible for generating source code in a specific language and for a specific FaaS platform.
  \item A module that performs the build of function containers.
  \item A platform-specific client that is required to deploy the collected functions.
\end{enumerate}

A schematic representation of the system using the Anzer platform is shown in Fig.~\ref{img:anzer-platfotm-scheme}.

\begin{figure}[ht] 
  \centering
  \includegraphics [scale=0.27] {anzer-platform-scheme.png}
  \caption{Schematic representation of the system.} 
  \label{img:anzer-platfotm-scheme}
\end{figure}

In the diagram, a square labelled ``Anzer-\textlambda'' illustrates the function implementing directly business logic of the application. This part is created by a developer by means of the selected programming language.   The code of the argument structure and function results are generated from the description at the first stage of the development.

The operations logic generated at the second stage interacts with the selected FaaS platform and also contains the structure code of argument and that of results. The container and the executable file compiled from the operations and business logic are illustrated in the figure by a square labelled ``\textlambda''.

The data bus, located in the figure below the functions, is an element of the FaaS platform, which is illustrated by a rectangle with the corresponding name. The position of the FaaS platform in the figure indicates its role in the system, which is to manage containers with collected functions and transfer data between them.

The biggest rectangle labelled ``Anzer Platform'' betokens that the functions are managed through the appropriate user interface. Despite this, it remains possible to use standard tools of the selected FaaS platform.

Languages with a manifest type system for which there is a compiler or static analyser with the possibility of static type checking in the code can act as the language of function implementation. Such languages may be, for example, C++, Golang, Java, TypeScript or PHP.

For Golang, for example, the check can be performed at the compilation stage due to strict typing.  If the compiler can bring the types defined in the business logic to the corresponding operations logic types, the function is considered to be correctly implemented.  The same logic is correct for Java and C++.

A different picture is for the interpreted PHP language. The latest version (PHP from version 7.1) includes the tools of strict typing, and community-created static analysis tools can identify violations of type compatibility before the start of the program.

However, support for a number of popular languages, such as JavaScript or Python, is not possible due to the lack of manifest typing.

So, the Anzer platform implements a language analyzer, client and libraries for the specific FaaS platform and the function building system. Its task is to verify correctness of the composition described in the language of Anzer, function building with checking the compliance of their implementation with the stated description and the deployment of functions in the FaaS platform. At the same time, to support a new programming language, it is necessary to develop a module for code generation and static analysis for this language and for all necessary FaaS platforms. To support a new platform, it is necessary to implement new modules for all necessary programming languages.

\subsection{Implementation}

The Apache OpenWhisk project has been selected as the first supported FaaS platform due to ease of the deployment, adequate performance~\cite{Shillaker2018} and the availability of the required functionality to implement the proposed solutions~\cite{Mohanty2018}.  Moreover, the development of a client for Apache OpenWhisk also involves the development of a client for the commercial IBM CloudFunctions platform, as the latter uses OpenWhisk as the basis.

The first target language used by the Anzer platform is Golang, which is supported by major cloud serverless service providers (AWS Lambda, Azure Functions, Google CloudFunctions). Due to the peculiarities of the type system of this language, namely duck typing, the implementation of translation of Anzer types into Golang ones appeared to be easier than a similar implementation for other languages. For example, JavaScript, which is considered the most popular among serverless technologies, does not have built-in tools for specifying and static type checking. In contrast to the language of Golang, which is a strictly and statically typed and infrastructure of which includes a compiler performing all type checking at the stage of compilation.  The following arguments support choosing Go as the first supported language: its simplicity, low threshold of entry and developed infrastructure.

Later, it is possible to support other languages with the possibility of static typing and supported by popular cloud environments, for example: Java, C\#,  TypeScript.

All platform modules are also implemented in the Golang language due to the above arguments and to reduce the number of languages and technologies used in the project at the first stage of development.

The user interface, like the whole project, has been developed in the Golang language and represents a utility with the CLI interface.  The utility uses a document in the Anzer language to generate the basis of a function, to build it and to deploy it in the FaaS platform.

Commands available to a user:

\begin{itemize}
  \item The command \lstinline{anzer validate} (\lstinline{anzer v}) provides the validation of the schema to check function compatibility without their build.
  \item The command \lstinline{anzer generate} (\lstinline{anzer g})  performs the generation of the function basics (the first stage of code generation).
  \item The command \lstinline{anzer export} (\lstinline{anzer e}) carries out schema validation, build of the executable files of functions and their export to the file system.
  \item The command \lstinline{anzer build} (\lstinline{anzer b}) provides schema validation, build of the executable files and function containers and their deployment in the FaaS platform.
\end{itemize}

According to the above standard order of work with the platform, \textbf{the validation of the written schema} is performed first of all. Validation is carried out by the command \lstinline{anzer v −i scheme.anz}, where \lstinline{-i} specifies the name of the schema file.

The language parser implemented using ANTLR project~\footnote{future-ref}, which enables to generate a lexer and parser for the developed language from its grammar description.

The next step is to \textbf{generate the function basics} by calling \lstinline{anzer g −i scheme.anz −o /go/src/a/a.go −f parse}. The \lstinline{-f} option defines the generation of the basis for the \lstinline{parse} function that appears in listing~\ref{lst:example}. The result will be saved in the file \lstinline{a.go} as specified by the \lstinline{-o} option.

The generated code of the future function contains only type descriptions and template for the handler function. This template is already functional, correct, and returns an empty value of a certain type by default. Anzer data types are converted to analogues in the language the function is developed. For example, for Golang, user-defined types are converted to structures; types with the \lstinline{Optional} constructor are converted to reference types, and the \lstinline{List} constructor is an analogue to a slice.

The generation of the base code of a new function is implemented by standard means of templating in Golang and placed in a separate module. At the same time, each language and each platform requires the development of a specific module designed to generate specific code. This is necessary because most platforms are incompatible with each other at the level of interaction between a runtime environment and a function, and there is no possibility of porting functions to a new platform without changing its interface.

When all the functions have been generated, developed, and loaded into the repository, the project should be built using the \lstinline{anzer b -i etl.anz -p wsk} command to \textbf{deploy the system}. The \lstinline{-p} option indicates the platform used, i.e. Apache OpenWhisk.

During the building process there is re-analysis of the source code in the Anzer language, which is necessary to verify the correctness of the composition schema and to generate the code for the functions to run in the FaaS platform. From the Anzer type descriptions, type descriptions in the target language and the service code required to run are regenerated. After the additional code is generated, the entire code is statically checked or compiled:  service code and business logic.  Static validation is performed for languages that do not have a compiler.  As a rule, third-party software is used for such verification. If the static analyser detects a mismatch between types from the service code and types from the business logic, the build stops along with the function deployment. This check is provided in case the developer changes the types in the function without changing them in the composition schema. In our case, the static check for Golang is performed by the compiler at the stage of the source file compilation.

The described analysis and function build can vary significantly depending on FaaS platforms and programming languages, and consequently, it requires different interpreters or compilers. To avoid this situation, these operations are performed in an isolated environment, in a Docker container. In the current execution, the same module, that is responsible for generation of a service code, also provides instructions for building the Docker container and compiling a function in it. After the Docker container finishes processing, there is a zip archive with a function’s executable file that can be uploaded to the FaaS platform, in our case -- in OpenWhisk.

Therefore, to build functions in any language for any platform, only the Docker containerization tool needs to be installed on a build system.

Interaction with the FaaS platform is also included in a separate module. As mentioned in chapter~\ref{chapter:subject_review}, there is the function composition solution for OpenWhisk called ``OpenWhisk Sequences''. The utility creates a sequence of functions that work as one, passing the results to the next one in the chain. The result of the last function is returned to the user in the HTTP response if the chain operation was initialized by the HTTP request.

The same chain of functions is created for each composition schema in the Anzer platform with the help of the OpenWhisk client.  Rules for running this chain for HTTP events, database events etc. can be configured by the OpenWhisk built-in tools.

%TODO: ??? here
In case of implementation of support for other platforms not providing functionality similar to OpenWhisk Sequences, it is necessary to implement automatic configuration of the next function call in each (?) individually. This implementation should be included in the module that deploys functions in the platform.

Finally, for local debugging, it can be used \lstinline{anzer e -i etl.anz -o ./export/ -p wsk -debug true}, which \textbf{builds functions and exports them to the file system}.

A partial example of a template that could be generated is shown in listing~\ref{lst:template}. Listing~\ref{lst:example} provides descriptions of the Anzer types for this template.

\begin{lstlisting}[caption=Generated Go-template, label=lst:template]
type TypeIn struct {
  Price float64 `json:"price"`
  Text  string  `json:"text"`
}
type TypeOut struct {
  Desc  string  `json:"desc"`
  Name  string  `json:"name"`
  Price float64 `json:"price"`
}

func Handle(input TypeIn) TypeOut {
  var out TypeOut
  return out
}
\end{lstlisting}

It is notable that there is no line length check in the given sample code. The specified data type constraint is checked by the Anzer interpreter, but the check in the function code is yet to be implemented.  After generating the template, the business logic in the \lstinline{Handle} function should be implemented.

If the function returns the type lstinline{Either Result Error}, the returned structure contains the lstinline{Left} and lstinline{Right} fields, as shown in listing~\ref{lst:gen_either}.

\begin{lstlisting}[caption=Code for the exception monad., label=lst:gen_either]
type TypeIn struct {
  Price float64 `json:"price"`
  Text  string  `json:"text"`
}
type TypeOut struct {
  Left *struct {
    Desc  string  `json:"desc"`
    Name  string  `json:"name"`
    Price float64 `json:"price"`
  } `json:"left"`
  Right *struct {
    Error string `json:"error"`
  } `json:"right"`
}
func Handle(input TypeIn) TypeOut {
  return handler(input)
}
\end{lstlisting}

Modifying the presented code, the programmer needs to determine which field to be filled, that is: either the \lstinline{Left} field, in case of the correct operation, or the \lstinline{Right} field in case of an error.

If the composition is executed with the \lstinline{bind} function (\lstinline{>>=}), then the basic logic is fixed in the generated operations logic.  In addition to the data types to be transferred, a condition, if the \lstinline{Right} field is empty, is added to it. If the field, responsible for storing an error, is not empty, the result is passed to the next function unchanged.

In summary, the Anzer platform is implemented in the Golang language, including the language parser, generator for one target language and client for OpenWhisk. The platform has been published on the Github portal\footnote{Anzer: Explicit and type-safe composition of cloud functions. -- URL: https://github.com/tariel-x/anzer; Access: 2019-05-12.}, considered fully functional and able to implement all the stated features in relation to the Golang programming language and  Apache OpenWhisk FaaS platform.

\section{Comparison with alternative solutions}
\label{chapter:anzer:comparison}

Comparison with similar solutions performing the composition was made using the criteria from~\cite{Garcia-Lopez2018}. The list of criteria is given below:

\begin{enumerate}
  \item ST-safeness criterion~\cite{Baldini-2017}, requiring the following conditions:
  \begin{itemize}
    \item Each composition should be a new function (principle of substitution).
    \item Each function should be a ``black box'' for the others and for the runtime environment.
  \end{itemize}
  \item Programming model: description of the method of solving the composition problem and the possibility of reflection at the level of composition.
  \item Support for parallel execution of functions.
  \item Restrictions on data transfer from one function to the next one.
  \item A way to store the source code of functions.
  \item Description of the composition architecture: the mechanism of the execution environment or the implementation of the composition using functions.
  \item The additional overhead computational costs.
  \item Payment model: the presence of additional financial costs for the use of the composition.
\end{enumerate}

The following is the description of the language and platform according to these criteria:

\begin{enumerate}
  %TODO: Anzer language is ST-safe.
  \item \textbf{ST-safeness}: the Anzer language satisfies the criterion because the composition of functions is considered a new function. Although the final implementation of the composition for different FaaS platforms may vary, the current implementation meets this requirement.
  \item \textbf{Programming model}: the solution model is a domain-specific language that supports the definition of error handling. However, the language does not have a means to dynamically control the flow of functions processing or any other way of reflection at the composition level.
  \item \textbf{Support for parallel function execution}: not implemented.
  \item \textbf{Restrictions on data transfer from one function to the next one}:  the criterion does not applicable to the language and platform of Anzer. However, a FaaS platform specifically used may have its own restrictions. In the OpenWhisk platform used in the study a limit is a configurable value. Commercial IBM CloudFunctions platform (compatible with OpenWhisk) has size limitation of 5 MB.
  \item \textbf{Characteristic of the composition architecture}: the developed tool is an external extension and uses available features of the selected FaaS platform.
  \item \textbf{Additional overhead computational costs}: not applicable to the developed solution, as Anzer is positioned as a universal platform that performs only type-safe composition of functions in the selected platform.  However, the current implementation uses the OpenWhisk Sequences composition tool, and the operations logic used with Anzer differs from one used in OpenWhisk.  However, the difference is only in the target structure of data deserialization, therefore the use of Anzer should not incur additional overhead costs.  Given the nature of the cloud platform and the OpenWhisk tools, checking for increased overhead computational costs is an impossible task. However, measuring the operation speed of the systems can indirectly confirm that Anzer does not incur such costs Experimental test of the hypothesis that the use of Anzer does not increase the system operation speed is presented in chapter~\ref{sec:anzer-aprobation-comparison}.
  \item \textbf{Payment model}:	not applicable to the developed solution The Apache OpenWhisk platform is free software designed for self-deployment and free of charge. However, the implementation for a specific FaaS platform may require additional financial costs.
\end{enumerate}

Description of alternative instruments according to the same criteria are given in the article~\cite{Garcia-Lopez2018}.
%TODO: кратко пересказать, что там написано

Chapter~\ref{sec:cloud_functions} defines the list of characteristics that are important in the development of systems using comparable solutions. Table~\ref{tbl:anzer_compisition_comparison} in Appendix~\ref{AppendixAnzerComparison} has the addition for table~\ref{tbl:compisition_comparison}s for the language and platform of Anzer.

According to the chart~\ref{tbl:anzer_compisition_comparison}, Anzer is inferior to alternative means. The developed language does not provide conditional statements, parallel processing. However, none of the found solutions for the function composition operates with types and performs function compatibility check.

\section{Chapter conclusions}

Chapter 3 has a description of the domain-specific language called Anzer, created to easily describe the types of data transmitted, type-safe composition of functions within the whole system, the semantics of their interaction and the logic of error handling. The language is a tool for visual and simple description, documentation and configuration of interaction of a cloud functions complex.

The developed software enables to automate the creation of new functions and maintain their integration with others at all stages of system development and support. To do this, the Anzer platform integrates with the required FaaS platform: performs the build of functions of the required format and their deployment. The Golang target language and Apache OpenWhisk platform are supported at the first stage. Together language and software prevent errors caused by a mismatch between input data format expected by a function and actually transmitted one.

The Anzer language and platform are inferior to alternative solutions according to the characteristics outlined in chapter~\ref{sec:cloud_functions}, as shown in the table~\ref{tbl:anzer_compisition_comparison} in Appendix~\ref{AppendixAnzerComparison}. However, the developed solution is the only one that provides a type-safe composition. Thus, the proposed solution is the only one of all considered in the paper that satisfies the purpose set in the Introduction.

\clearpage