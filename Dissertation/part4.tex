\chapter{Testing of Anzer and further work} \label{chapter:approbation}

The chapter describes the testing, in the course of which Anzer was used to create the subsystem related to the software development infrastructure. Based on the successful test implementation, the solution was implemented in operation during the development of one of the subsystems of the Youla project (the Mail.ru Group company's project).

The use of Anzer in the operating project revealed a number of drawbacks, such as the inability to describe parallel operation and the lack of caching of the results of function build . In this regard, the prospects for the language and platform development and description of the potential implementation of new functionality have been described.

The last part of the chapter discusses the confirmation of the hypothesis that the use of the Anzer platform does not create additional costs of computing power compared to the operation of the system built without use of the Anzer platform.

\section{Creating a system for the appointment of a developer responsible for code review}

For testing purposes, the solution was used to create one of the infrastructure systems that simplifies the programmers’ work.

Modern organization of team development of software products involves the use of a version control system, a central repository of projects, systems of automatic compilation, assembly, deployment, etc.  One of a programmer’s tasks in the team work is to assess the quality of colleagues’ code (that is ``code review''). However, a human factor sometimes negatively affects the speed of this process: the employees forget about code evaluation requests or don't want to be distracted
from current tasks. To solve these problems, some teams practice assigning a responsible developer to review the code.

The main task of the developed system is to automatically, if necessary, assign a person responsible for checking the code and notify this person about the appointment.  Its another task is to determine the most relevant candidate according to candidate’s competence based on the source code contained in the repository

The example uses the Gitlab version control system as a notification method: e-mail and instant message system (instant messanger, IM) called Slack. Since the number of employees does not exceed twenty people, in order to make it simpler, in the first iteration the list of employees along with the list of competence is contained in the source code of the function.

The system consists of three functions which perform:

\begin{enumerate}
  \item Receiving an event from the project repository, validation, defining the main programming language in the repository;
  \item Choosing a responsible developer;
  \item Assigning the responsible developer and sending him a notification.
\end{enumerate}

The system is graphically illustrated in figure~\ref{img:approbation1-scheme}. The rectangles with numbers correspond to the function numbers from the list above.

\begin{figure}[ht] 
  \centering
  \includegraphics [scale=0.7] {approbation1.png}
  \caption{Schematic representation of the system implemented.} 
  \label{img:approbation1-scheme}
\end{figure}

An example of the schema used in testing is given in listing~\ref{lst:aprobation1}. The \lstinline{Hook} type describes a small part of the request that the Gitlab project management system automatically sends when certain events occur. The \lstinline{Event} type contains validated information about edits in code in some repository. The \lstinline{Assignment} type is used to transfer information about the person assigned to control the current edits in the code.

Function \lstinline{validate}, in accordance with its title, checks an incoming query and defines the programming language used in the repository by means of Gitlab API. The \lstinline{assign} function appoints a person responsible for checking code edits based on the programming language of the repository. The \lstinline{notify} function sends an appointment notification to the chat. 

\begin{lstlisting}[language=haskell, caption=Example of the use of Anzer., label=lst:aprobation1]
type Hook = {
  user :: {
    username :: String
  }
  repository :: {
    name     :: String
    homepage :: String
  }
}
type Event = {
  author     :: String
  repository :: String
  language   :: String
}
type Assignment = {
  reviewer   :: String
  repository :: String
}

github.com/u/validate[go]:: 
  Hook -> Event
1github.com/u/assign[go]:: 
  Event -> Assignment
github.com/u/notify[go]:: 
  Assignment -> Bool

assign_mr = validate . assign . notify
invoke (
  assign_mr,
)
\end{lstlisting}

This example is a very simple system that does not use, for example, the possibility to handle errors. Nevertheless, its creation made it possible to verify the viability of the approach at the minimum level.

\section{The use of Anzer in the ``Youla'' project}

Based on the testing of the presented solution described in the previous paragraph, its implementation in the Youla project was continued. The report on the results of implementation is presented in Appendix~\ref{AppendixAct}.

The ``Youla'' project is a platform for advertising goods sold by individuals and legal entities. The task of the developed component is to receive information from providers about cars sold, the transformation and supplement of data, placement in the project's database.  Data transformation involves changing the format and grouping according to certain criteria. Data supplement involves finding relevant photos in the internal directory.

Data from the provider comes in XML files. Supplement of data with relevant photos is performed by separate services having HTTP interface.

Based on the above tasks, the subsystem can be divided into components that solve the following tasks:

\begin{itemize}
  \item Validation of incoming data and its transformation into a model used in the project.
  \item Grouping the data according to certain criteria.
  \item Detection of deleted or modified blocks of the data grouped.
  \item Supplement of advertisements with relevant photos.
  \item Registration of a new advertisement in the database.
\end{itemize}

Each component is placed in a separate cloud function. The general structure of the system is shown in Fig.~\ref{img:approbation2-scheme}.

\begin{figure}[ht] 
  \centering
  \includegraphics [scale=0.5] {approbation2.png}
  \caption{Schematic representation of the data transformation and loading system.} 
  \label{img:approbation2-scheme}
\end{figure}

The functions were implemented in the Golang language with the help of Anzer and arranged in a flow. In the diagram they are represented by rectangles with the symbol ``\textlambda''.

The \lstinline{transform} function downloads the XML file from a provider's server and \textbf{transforms the data} into the internal model. Standard tools and libraries of Golang are used for loading and transforming. The result is an array of advertisements in the internal format.

The \lstinline{group} function is used to \textbf{group data} by a number of criteria. The essence of its work is to find goods with duplicate characteristics and combine them into groups with the indication of the total number and original identifiers.  The result is an array of advertisements in the internal format with the indication of initial number.

The \lstinline{difference} function \textbf{detects changes in the data file}: which items have been loaded for the first time, which have been removed from the provider file, and which have changed. To detect these changes, the history in the database is used. The function marks an advertisement with the detected status (new, deleted, changed, not changed).

In order for further data processing to be performed in parallel for each advertisement, it is necessary not only to pass the result of the work to the next function, but also to run several execution threads.  To do this, during the work there is the initialization of the OpenWhisk trigger for every advertisement with the data of this advertisement; the trigger is specified when the function is developed. The OpenWhisk platform, when the trigger is activated, starts the function bound, which is the first in the further chain. In the figure this process is indicated by the ``N triggers'' inscription. The return result of the function is the statistics for added, changed, and deleted advertisements.

The \lstinline{images} function is a triggered function of the second flow; it's task is to \textbf{search for relevant photos}. The search is performed calling for the third-party HTTP API. The result of the call is added to the advertisement.

The finished advertisement is received by the \lstinline{save} function, which \textbf{saves it to the system database}. Work with the database is also carried out using a third-party service.

The last \lstinline{result} function checks if all previous functions have completed successfully. If an error occurs, the error information is written to a specialized log.

The interaction schema of listed functions is presented in Appendix~\ref{AppendixYoulaComposition} in listing~\ref{list:youla-composition}. It is worth noting that due to the lack of a mechanism for the parallel task description there are two compositions in the schema:  to process all data as a whole and to process each advertisement separately.   The file downloading composition is initialized by the OpenWhisk platform scheduling mechanism. This point is marked with the ``cron'' inscription in the schema. The composition of individual advertisement processing starts when the trigger is initialized. In this case, the connection with the trigger is manually configured by the developer. The need for manual configuration of the communication is due to the lack of tools to describe parallel tasks in the Anzer language.

During the development, the insufficient speed of the system build was also revealed, due to the fact that the \lstinline{anzer build} command builds all the functions described in the schema. A full build is performed for all functions every time a change is made, even for those that have not been changed in the code or in the composition schema.  A flexible build of only modified functions would speed up the process of development by means of Anzer.

As a result, except for the lack of a way to run functions in parallel without third-party tools and the lack of caching build results, the Anzer language and platform are suitable for forming a type-safe composition of functions. The system successfully operates, and typing errors are excluded by the solution used. Using the interaction schema along with other documentation facilitates the transmission of knowledge about the system to new developers. Thus, the experience of implementation made it possible to verify the viability and prospects of the approach. However, the use of the trigger mechanism, which is configured separately, indicates the need to refine the solution.

\section{Future development}

Based on the experience gained during the implementation, the need to develop additional platform functions and language capabilities was revealed. As noted earlier, the language needs a tool to identify tasks running in parallel. The described necessary functionality is yet to be developed.

It should also be noted there is still a need to add support for other programming languages: TypeScript, PHP and others that meet the criteria noted in paragraph~\ref{chapter:anzer:structure}.

In addition to the definition of parallel work and support for new languages, there are other prospects for improving the developed solution. The main directions are described further.

\subsection{Caching of build results}
\label{chapter:approbation:cache}

Using the platform revealed the need to cache the build results.  The problem is that all functions are rebuilt every time the project is built, which significantly slows down the development process. The adaptation of caching a list of dependencies and their versions, inherent to many package managers of programming languages, is proposed as a solution.

It is suggested to calculate and store the hash sum of the function description in a specialized file called ``\lstinline{anz.sum}''. In addition to the hashing of the description in the Anzer language it is necessary to fix a function version. For this purpose  an \lstinline{anz.sum} file will store the hash sum of the Git repository commit for each function. The supposed content of the \lstinline{anz.sum} file is shown in listing~\ref{lst:anzsum1}.

\begin{lstlisting}[caption=Example of the anz.sum file, label=lst:anzsum1]
github.com/u/validate 77ae17b0 fbdef35a632091da5a6a3ed9fe9aea250a767851
github.com/u/notify 23fd36b6 cfacc5d400a85d8c9ed05b0fb6c112dea31c95ff
\end{lstlisting}

In the example, in each line there is the function specified first, followed by the hash-sum of its definition in the Anzer language.  The example uses the hash-function ``CRC32'' due to the small size of the result. However, when implementing the described functionality, the appropriate hash function will be specified. The last part of each line is the commit ID (hash sum) in the Git repository.

In order for the anzer function to be built with a given version of the source code, the set Git identifier must be defined when generating instructions for the Dockerfile. The result of the function build is cached in a dedicated directory in the file system.  It is assumed that the versions of functions will be fixed and will be updated only at the request of a separate command.

The algorithm for working with versions is shown in figure~\ref{img:sum_algorithm}.

\begin{figure}[ht] 
  \centering
  \includegraphics [scale=0.65] {sum-algorithm1.png}
  \caption{Algorithm of the function building using a sum file.} 
  \label{img:sum_algorithm}
\end{figure}

Introduction of the proposed functionality will speed up the build of intermediate versions of functions, provide function versioning.

\subsection{Conditional statements and pattern matching in the language}
\label{chapter:approbation:pattern-matching}

Another extension that was not in demand in the implementation process, but seems useful, is matching with a template. The possibility of the language to determine control flow based on intermediate data is inherent to many alternative solutions. The introduction of the same feature in the language Anzer would increase the range of ways how to use the application.

An example of the possible extension of the Anzer language with pattern matching is shown in listing~\ref{lst:pattern-matching}.

\begin{lstlisting}[language=haskell, caption=Example of pattern matching in Anzer, label=lst:pattern-matching]
type T1 = {
  f :: String
}
type T2 = {
  f :: String
}
type Error = {
  err :: String
}
github.com/u/f1[go]:: T1 -> T2 | Error
github.com/u/f2[go]:: T2 -> _
github.com/u/f3[go]:: Error -> _
cond T2 = f2
cond Error = f3
seq = f1 . cond
invoke (
  seq,
)
\end{lstlisting}

In this example, four functions are defined.

\begin{itemize}
  \item \lstinline{f1} returns either a result of type \lstinline{T2} or an error \lstinline{Error};
  \item \lstinline{f2} works only with data of type \lstinline{T2} and performs some action;
  \item \lstinline{f3} is a handler of a error of type \lstinline{Error};
  \item \lstinline{cond} is defined for types \lstinline{T2} and \lstinline{Error}. In the first case, it is defined as a synonym for the function \lstinline{f2}, and as a synonym for \lstinline{f3} in the second case.
\end{itemize}

For the OpenWhisk platform, it is proposed to implement conditions of the \lstinline{cond} type as service functions that compare function arguments with data types and initialize triggers to which certain functions are subscribed.

Using the pattern-matching construction will enable more flexible configuration of the control flow and will bring Anzer closer to alternative solutions in terms of functionality.

\section{Comparison of performance of systems using Anzer with systems using OpenWhisk standard tools}
\label{sec:anzer-aprobation-comparison}

Chapter~\ref{chapter:anzer:comparison} hypothesized that the use of Anzer does not increase the time for system operation.  The experiment was conducted to confirm the hypothesis. The essence of the experiment is to measure the execution time of the same function flow created by means of Anzer in one case and by means of standard tools of the serverless platform in another one. In our case, the OpenWhisk platform is used as the only one supported.

To conduct the experiment, test flow of actions was created, the purpose of which was to transform and supplement the data. The sequence consists of four functions.

\begin{enumerate}
  \item The \lstinline{transform} function is for data validation and transformation.
  \item The \lstinline{load_photos} function performs simulated data load.
  \item The \lstinline{supplement} function performs simulated data supplement.
  \item The \lstinline{save} function performs data saving in the database.
\end{enumerate}

All of these functions only simulate business logic, as it is necessary to exclude the influence of external services on the speed of work for the accuracy of the experiment. 

The flow of actions was implemented in two ways in the most similar vein. The example of the \lstinline{load_photos} function using Anzer is shown in Appendix~\ref{AppendixLoadPhotosAnzer}. Without Anzer –- in Appendix~\ref{AppendixLoadPhotosPlain}. The sequence is implemented by the OpenWhisk standard tools and deployed with the \lstinline{wskdeploy} standard utility. Functions run in the IBM CloudFunctions cloud platform and are available for running by means of HTTP REST API.

Two HTTP requests with different data size were formed for the experiment. Each request was executed five hundred times (500) in both flows. Requests were executed sequentially, one request per time unit. Other requests were not sent to the system during the experiment.  The result of each measurement is the response time of the function flow to a request, measured in milliseconds. Thus, during the experiment four sets consisting of four thousand (4000) measurements were obtained.

To obtain the execution time of each request, the program has been developed that exports the results to CSV format. Already existing tools (such as Apache Benchmark) were not used because they don’t enable to save data on runtime for each request. Source codes of both projects and the results of the measurements are available in the repository Gitlab~\footnote{Anzer benchmark, URL: https://gitlab.com/anzer-cloud/benchmark ; Access 29-07-2019.}.

Based on the analysis of the data obtained, the result indicated that 95\% of the requests for the first test request had been performed for 511 milliseconds for both the system developed with the use of Anzer, and system without it. For the second test request, 95\% of requests had been made for 613 milliseconds.

Consequently, the experiment and analysis of the results confirm the hypothesis that the use of Anzer does not introduce additional delays in the operation of the system developed.

\section{Chapter conclusions}

The chapter describes the operation and application of the proposed solution for several tasks. The use of the Anzer language and platform has shown the possibility of their application in practice and excludes errors associated with the type mismatch when cloud functions communicate. In addition, the composition schema along with other documentation facilitates the transmission of knowledge about the system to new developers. 

However, the identified shortcomings make it difficult to further implement the solution in the project. Among the disadvantages there is the lack of support for parallel execution, the lack of results caching.

Based on the information obtained, several proposals for the further development of the project were put forward:

\begin{enumerate}
  \item As noted above, it is needed to configure the parallel execution of tasks using the composition schema. Possible implementation of the language extension is being studied.
  \item It is necessary to develop caching of build results. This will avoid the rebuild of all functions in case of any schema updating, and will provide the flexibility to choose the appropriate version of the function source code.
  \item Implementation of the pattern matching in the language, which will allow to choose control flow based on the intermediate data needs to be done. This improvement will increase the flexibility of schemas and the range of the solution’s application.
  \item Using the project as a tool to create full-fledged commercial products requires the development of specialized tools for local debugging and function testing. At the moment, such tools are not available.
\end{enumerate}

Besides the implementation of the developed solution in the Youla project, performance analysis of the work of  the OpenWhisk serverless system with the use of Anzer and without it was made.  The analysis revealed the absence of differences in the speed of the systems’ work.

In general, the use of Anzer together with the accompanying software facilitates the creation of new systems and support of existing ones, while not affecting the system requirements for hardware resources and performance.   Using the solution excludes situations of type mismatch and achieves the goal specified in the Introduction.
